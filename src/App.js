import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload. Okay.
          </p>
          <a
            className="App-link"
            href="#/test"
          >
            Unmount Application
          </a>
        </header>
      </div>
    );
  }
}

export default App;
