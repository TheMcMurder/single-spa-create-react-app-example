import './index.css';
import * as serviceWorker from './serviceWorker';
import * as singleSpa from 'single-spa'
import * as singleSpaApplication from './single-spa.js'

// single-spa stuff
singleSpa.registerApplication('root', singleSpaApplication, activityFn)

function activityFn(location) {
  return location.hash === ''
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
singleSpa.start();
